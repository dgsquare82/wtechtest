//
//  DogListRouterMock.swift
//  WTechTestTests
//
//  Created by Jae Kwang Lee on 2020/02/19.
//  Copyright © 2020 Jae Kwang Lee. All rights reserved.
//

import Foundation
import UIKit
@testable import WTechTest

class DogListRouterMock: DogListRouterProtocol {
    var isPushToDogDetail: Bool = false

    func pushToDogDetail(id: String, view: UIViewController) {
        self.isPushToDogDetail = true
    }
    
    static func createModule() -> UIViewController {
        return UIViewController()
    }
}

