//
//  DogListProviderFile.swift
//  WTechTest
//
//  Created by Jae Kwang Lee on 2020/02/17.
//  Copyright © 2020 Jae Kwang Lee. All rights reserved.
//

import Foundation
@testable import WTechTest

class DogListProviderMock: DogListProviderProtocol {
    private var dictionary: [[String: Any]]!
    private var isAlwaysFail: Bool
    
    init(dictionary: [[String: Any]], isAlwaysFail: Bool = false) {
        self.dictionary = dictionary
        self.isAlwaysFail = isAlwaysFail
    }
        
    func loadList(completion: completionClosure) {
                
        if isAlwaysFail == true {
            completion?(.error(.responseIsNotValid))
            return
        }
        else {
            let decoder = DictionaryDecoder()
            guard let dogs = try? decoder.decode([DogData].self, from: self.dictionary as Any) else {
                completion?(.error(.networkError))
                return
            }

            completion?(.response(dogs))
        }
    }
}

