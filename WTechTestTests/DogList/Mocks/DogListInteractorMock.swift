//
//  DogListInteractorMock.swift
//  WTechTestTests
//
//  Created by Jae Kwang Lee on 2020/02/19.
//  Copyright © 2020 Jae Kwang Lee. All rights reserved.
//

import Foundation
@testable import WTechTest

class DogListInteractorMock: DogListInteractorProtocol {
    var presenter: DogListPresenterProtocol?
    var isLoadingSuccessful = true

    var isLoadList: Bool = false
    var isSortList: Bool = false
    
    init(isLoadingSuccessful: Bool = true) {
        self.isLoadingSuccessful = isLoadingSuccessful
    }
    
    func loadList(sortBy: DogListSortType) {
        self.isLoadList = true
        if self.isLoadingSuccessful {
            presenter?.onListLoaded(dogs: [])
        }
        else {
            presenter?.onListLoadFalied()
        }
    }
    
    func sortList(sortBy: DogListSortType) {
        self.isSortList = true
        presenter?.onListLoaded(dogs: [])
    }
}
