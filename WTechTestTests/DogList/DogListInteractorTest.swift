//
//  DogListInteractorTest.swift
//  WTechTestTests
//
//  Created by Jae Kwang Lee on 2020/02/17.
//  Copyright © 2020 Jae Kwang Lee. All rights reserved.
//

import XCTest
@testable import WTechTest

class DogListInteractorTest: XCTestCase {

    override func setUp() {
    }

    override func tearDown() {
    }

// MARK: - Internal Logic Tests
    func testParsingLifespan() {
        
        var tuple: (Int, Int)?
        tuple = DogListInteractor.parseLifeSpan(lifeSpanFormatted: "")
        XCTAssert(tuple == nil)

        tuple = DogListInteractor.parseLifeSpan(lifeSpanFormatted: "6 - 12 test")
        XCTAssert(tuple == nil)

        tuple = DogListInteractor.parseLifeSpan(lifeSpanFormatted: "6 * 12 years")
        XCTAssert(tuple == nil)

        tuple = DogListInteractor.parseLifeSpan(lifeSpanFormatted: "6 years")
        XCTAssert(tuple == nil)

        tuple = DogListInteractor.parseLifeSpan(lifeSpanFormatted: "6 - 12 years")
        XCTAssert(tuple?.0 == 6 && tuple?.1 == 12)
    }


// MARK: - Module Test
    func testLoadListCallsPresenterLoadFailError() {
        let mockProvider = DogListProviderMock(dictionary: [], isAlwaysFail: true)
        let interactor = DogListInteractor(provider: mockProvider)
        let mockPresenter = DogListPresenterMock(interactor: interactor)
        interactor.presenter = mockPresenter

        interactor.loadList(sortBy: .lifespan(isAscending: true))

        let expectation = self.expectation(description: "When the interactor call fails, loadList should call onListLoadFailed.")

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1.0)

        XCTAssertTrue(mockPresenter.isOnListLoadFalied)
    }

    func testLoadListCallsPresenterisOnListLoaded() {
        let mockProvider = DogListProviderMock(dictionary: [])
        let interactor = DogListInteractor(provider: mockProvider)
        let mockPresenter = DogListPresenterMock(interactor: interactor)
        interactor.presenter = mockPresenter

        interactor.loadList(sortBy: .lifespan(isAscending: true))

        let expectation = self.expectation(description: "When the interactor call succeeds, loadList should call onListLoaded.")

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1.0)

        XCTAssertTrue(mockPresenter.isOnListLoaded)
    }

    func testSortListCallsPresenterOnListLoaded() {
        let mockProvider = DogListProviderMock(dictionary: [])
        let interactor = DogListInteractor(provider: mockProvider)
        let mockPresenter = DogListPresenterMock(interactor: interactor)
        interactor.presenter = mockPresenter

        let expectation = self.expectation(description: "SortList should call onListLoaded")

        interactor.sortList(sortBy: .lifespan(isAscending: true))

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1.0)

        XCTAssertTrue(mockPresenter.isOnListLoaded)
    }

    func testLoadListPerformsDataAscending() {
        let mockDictionary = [
            ["id": "A", "url": "testUrl", "breeds": [["name": "American Bully", "bred_for": "", "life_span": "8 - 15 years", "temperament": ""]]],
            ["id": "B", "url": "testUrl", "breeds": [["name": "Australian Bully", "bred_for": "", "life_span": "5 - 7 years", "temperament": ""]]]
        ]

        let mockProvider = DogListProviderMock(dictionary: mockDictionary)
        let interactor = DogListInteractor(provider: mockProvider)
        let mockPresenter = DogListPresenterMock(interactor: interactor)
        interactor.presenter = mockPresenter

        let expectation = self.expectation(description: "LoadList with lifespan(ascending) should ascend dog items.")

        interactor.loadList(sortBy: .lifespan(isAscending: true))

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1.0)

        XCTAssertTrue(interactor.dogs[0].id == "B")
        XCTAssertTrue(interactor.dogs[1].id == "A")
    }

    func testSortListPerformsDataDescending() {
        let mockDictionary = [
            ["id": "A", "url": "testUrl", "breeds": [["name": "American Bully", "bred_for": "", "life_span": "8 - 15 years", "temperament": ""]]],
            ["id": "B", "url": "testUrl", "breeds": [["name": "Australian Bully", "bred_for": "", "life_span": "5 - 7 years", "temperament": ""]]]
        ]

        let mockProvider = DogListProviderMock(dictionary: mockDictionary)
        let interactor = DogListInteractor(provider: mockProvider)
        let mockPresenter = DogListPresenterMock(interactor: interactor)
        interactor.presenter = mockPresenter

        let expectation = self.expectation(description: "LoadList with lifespan(descending) should descend dog items.")

        interactor.loadList(sortBy: .lifespan(isAscending: true))

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            interactor.sortList(sortBy: .lifespan(isAscending: false))
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1.0)

        XCTAssertTrue(interactor.dogs[0].id == "A")
        XCTAssertTrue(interactor.dogs[1].id == "B")
    }
}
