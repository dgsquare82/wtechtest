
//
//  Constant.swift
//  WTechTest
//
//  Created by Jae Kwang Lee on 2020/02/21.
//  Copyright © 2020 Jae Kwang Lee. All rights reserved.
//

import Foundation

struct Constant {
    static let BaseDogAPIPath: String = "https://api.thedogapi.com/"
}
