//
//  DogListProtocols.swift
//  WTechTest
//
//  Created by Jae Kwang Lee on 2020/02/17.
//  Copyright © 2020 Jae Kwang Lee. All rights reserved.
//

import Foundation
import UIKit

protocol DogListViewProtocol: class {
    var presenter: DogListPresenterProtocol! { get set }
    
    func updateView(cellModels: [DogListViewCellModel], sortText: String)
    func showLoading()
    func hideLoading()
    func showAlert(titie: String, message: String)
}

protocol DogListPresenterProtocol: class {
    var interactor: DogListInteractorProtocol { get set }
    var view: DogListViewProtocol? { get set }

    func viewDidLoad()
    
    func onListLoaded(dogs: [DogItem])
    func onListLoadFalied()

    func onItemClicked(id: String)
    func onSortClicked()
}

protocol DogListInteractorProtocol: class {
    var presenter: DogListPresenterProtocol? { get set }
    
    func loadList(sortBy: DogListSortType)
    func sortList(sortBy: DogListSortType)
}

protocol DogListRouterProtocol: RouterProtocol {
    func pushToDogDetail(id: String, view: UIViewController)
}

