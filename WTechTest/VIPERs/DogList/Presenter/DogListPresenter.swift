//
//  DogListPresenter.swift
//  WTechTest
//
//  Created by Jae Kwang Lee on 2020/02/17.
//  Copyright © 2020 Jae Kwang Lee. All rights reserved.
//

import Foundation
import UIKit

struct DogListViewCellModel {
    let id: String
    let name: String
    let photoURL: String
    let lifespanFormatted: String
    let desc: String
}

class DogListPresenter: DogListPresenterProtocol {
    var interactor: DogListInteractorProtocol
    var router: DogListRouterProtocol
    
    weak var view: DogListViewProtocol?

    var sortType: DogListSortType = .lifespan(isAscending: true)
    
    init(interactor: DogListInteractorProtocol, router: DogListRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

    func viewDidLoad() {
        self.view?.showLoading()
        self.interactor.loadList(sortBy: self.sortType)
    }

    func onListLoaded(dogs: [DogItem]) {
        let cellModels = dogs.map { (item) -> DogListViewCellModel in
            let desc = item.bredFor
            return DogListViewCellModel(id: item.id, name: item.name, photoURL: item.photoURL, lifespanFormatted: item.lifespanFormatted, desc: desc)
        }

        self.view?.updateView(cellModels: cellModels, sortText: self.getSortText(sortType: self.sortType))
        self.view?.hideLoading()
    }
    
    func onListLoadFalied() {
        self.view?.showAlert(titie: "Error", message: "Please try it again.")
        self.view?.hideLoading()
    }
    
    func onItemClicked(id: String) {
        //Todo: Let a router to handle this.
        if let viewController = self.view as? UIViewController {
            self.router.pushToDogDetail(id: id, view: viewController)
        }
    }
    
    func onSortClicked() {
        switch sortType {
        case .lifespan(let isAscending):
            self.sortType = .lifespan(isAscending: !isAscending)
        }
        self.interactor.sortList(sortBy: self.sortType)
    }
}

// Internal Logics
extension DogListPresenter {
    fileprivate func getSortText(sortType: DogListSortType) -> String {
        var text: String
        switch(sortType) {
        case .lifespan(let isAscending):
            text = (isAscending) ? "Lifespan ▲" : "Lifespan ▼"
        }
        return text
    }
}

