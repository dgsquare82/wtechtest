//
//  DogListProviderREST.swift
//  WTechTest
//
//  Created by Jae Kwang Lee on 2020/02/18.
//  Copyright © 2020 Jae Kwang Lee. All rights reserved.
//

import Foundation

class DogListProviderREST: DogListProviderProtocol {
    func loadList(completion: completionClosure) {
        DogAPIClient.sharedInstance().loadList { (response) in
            switch (response) {
            case .response(let dict):
                let decoder = DictionaryDecoder()
                guard let dogs = try? decoder.decode([DogData].self, from: dict as Any) else {
                    completion?(.error(.responseIsNotValid))
                    return
                }
                completion?(.response(dogs))
            case .error(_):
                completion?(.error(.networkError))
            }
        }
    }
}
