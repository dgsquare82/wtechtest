//
//  NSObject+ClassName.swift
//  WTechTest
//
//  Created by Jae Kwang Lee on 2020/02/18.
//  Copyright © 2020 Jae Kwang Lee. All rights reserved.
//

import Foundation

extension NSObject {
    var className: String {
        return String(describing: type(of: self))
    }
    
    class var className: String {
        return String(describing: self)
    }
}

